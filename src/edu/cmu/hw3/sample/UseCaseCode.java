package edu.cmu.hw3.sample;

import edu.cmu.hw3.library.BetterThread;
import edu.cmu.hw3.library.CancellableThread;

import java.util.concurrent.TimeUnit;

public class UseCaseCode {
    public static void main(String[] args) {
        accessibilityViolation();
        fixedAccessibilityViolation();

        boilerplateSimpleSleep();
        simpleSleep();

        boilerplateCancel();
        simpleCancel();

        bloatedStaticUtilities();

        nonFluentSetters();
        fluentSetters();
    }
    public static void accessibilityViolation() {
        Thread t = new Thread(() -> System.out.println("I'm an instance"));
        t.run(); // bad - No IntelliJ warning
        t.start(); // good
    }

    public static void fixedAccessibilityViolation() {
        BetterThread b = new BetterThread(() ->
                System.out.println("I'm an instance"));
        // b.run(); /// Does not compile
        b.start();
    }

    public static void boilerplateSimpleSleep() {
        try {
            Thread.sleep(1000);
        } catch(InterruptedException e) {
            throw new AssertionError("Impossible");
        }
    }

    public static void simpleSleep() {
        BetterThread.Current.sleep(1000, TimeUnit.MILLISECONDS);
    }

    public static void boilerplateCancel() {
        class BadBlinker implements Runnable {
            private volatile Thread thread;

            public void start() {
                thread = new Thread(this);
                thread.start();
            }

            public void stop() {
                thread = null;
            }

            public void run() {
                Thread thisThread = Thread.currentThread();
                while (thread == thisThread) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e){
                        throw new AssertionError("Impossible");
                    }
                    System.out.println("repainting screen");
                }
            }
        }

        BadBlinker blinker = new BadBlinker();
        blinker.start();

        Thread control = new Thread(()-> {
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                throw new AssertionError("Impossible");
            }
            // This is a hack because blinker.stop() is implemented by
            // mutating internal state.
            Thread thread = blinker.thread;
            blinker.stop();
            try {
                thread.join();
            } catch (InterruptedException e) {
                throw new AssertionError("Impossible");
            }
            System.out.println("Successfully cancelled");
        });
        control.start();
        try {
            control.join();
        } catch (InterruptedException e) {
            throw new AssertionError("Impossible");
        }
    }

    public static void simpleCancel() {
        CancellableThread blinker = new CancellableThread((token) -> {
            while(!token.isCancelled()) {
                BetterThread.Current.sleep(1000, TimeUnit.MILLISECONDS);
                System.out.println("repainting screen");
            }
        });
        blinker.start();

        BetterThread control = new BetterThread(() -> {
            BetterThread.Current.sleep(10, TimeUnit.SECONDS);
            blinker.cancel();
            assert(blinker.isCancelled());
            // At this point we know that the instance is being cancelled, but
            // it may not be cancelled yet, so we need to wait until the
            // instance is not alive before declaring success
            blinker.join();
            System.out.println("Successfully Cancelled");
        });
        control.start();
        control.join();
    }

    public static void bloatedStaticUtilities() {
        Thread.UncaughtExceptionHandler eh = Thread.getDefaultUncaughtExceptionHandler();
        Thread.currentThread().setPriority(Thread.NORM_PRIORITY);
        Thread[] array = new Thread[10];
        Thread.enumerate(array);
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        ThreadGroup group = Thread.currentThread().getThreadGroup();
    }

    public static void nonFluentSetters() {
        Thread thread = new Thread(() -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                throw new AssertionError("Impossible");
            }
        });
        thread.setName("my thread");
        thread.setDaemon(false);
        thread.setUncaughtExceptionHandler((t, e) ->
                System.out.println("exception on" + t.getName()));
        thread.start();
    }

    public static void fluentSetters() {
        BetterThread thread =
                new BetterThread(() -> BetterThread.Current.sleep(1000, TimeUnit.MILLISECONDS))
                        .setName("my thread")
                        .setDaemon(false)
                        .setUncaughtExceptionHandler((t, e) ->
                                System.out.println("exception on" + t.getName()));
        thread.start();
    }


}
