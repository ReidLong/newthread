package edu.cmu.hw3.tests;

import edu.cmu.hw3.library.BetterThread;

import edu.cmu.hw3.library.CancellableThread;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

public class BetterThreadTest {
    @Test
    public void basicTest() {
        BetterThread b = new BetterThread(() -> {
            BetterThread.Current.dumpStack();
            System.out.println(Arrays.toString(BetterThread.Current.instance().getStackTrace()));
        }, "My Thread");
        Assert.assertEquals("My Thread", b.getName());
        String expected = String.format("BetterThread[My Thread,%d]",
                b.getId());
        Assert.assertEquals(expected, b.toString());
        b.setName("Foo");
        Assert.assertEquals("Foo", b.getName());

        Assert.assertFalse(b.isDaemon());
        b.setDaemon(true);
        Assert.assertTrue(b.isDaemon());

        Assert.assertEquals(null, b.getUncaughtExceptionHandler());
        BetterThread.UncaughtExceptionHandler eh = (t, e) -> {};
        b.setUncaughtExceptionHandler(eh);
        Assert.assertEquals(eh, b.getUncaughtExceptionHandler());

        b.start();
    }

    @Test
    public void joinTest() {
        BetterThread b = new BetterThread(() -> Assert.assertTrue(BetterThread.Current.instance().isAlive()));
        Assert.assertFalse(b.isAlive());
        b.start();
        b.join();
        Assert.assertFalse(b.isAlive());
    }

    @Test
    public void joinTestWithTimeout() {
        final long WAIT_TIME = 100;
        final long TOLERANCE = 10;
        CancellableThread b = new CancellableThread(status -> {
            while (!status.isCancelled())
                BetterThread.Current.yield();
        });
        b.start();
        long begin = System.currentTimeMillis();

        b.join(WAIT_TIME, TimeUnit.MILLISECONDS);
        long finish = System.currentTimeMillis();
        long delta = WAIT_TIME - finish + begin;
        // We wanted roughly 100 milliseconds waited
        Assert.assertTrue(delta > -TOLERANCE && delta < TOLERANCE);
        // cleanup
        b.cancel();
    }

    @Test
    public void lockTest() {
        synchronized (this) {
            Assert.assertTrue(BetterThread.Current.holdsLock​(this));
            BetterThread thread = new BetterThread(() ->
                    Assert.assertFalse(BetterThread.Current.holdsLock​(this)));
            thread.start();
            thread.join();
        }
    }
}