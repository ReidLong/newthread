package edu.cmu.hw3.tests;

import edu.cmu.hw3.library.BetterThread;
import edu.cmu.hw3.library.CancellableThread;
import org.junit.Assert;
import org.junit.Test;

import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class CancellableThreadTest {
    @Test
    public void TestCancellable() throws ExecutionException {
        CancellableThread b = new CancellableThread(status -> {
            while (!status.isCancelled())
                BetterThread.Current.yield();
        });
        b.start();
        b.cancel();
        // Will not join if cancel does not work
        try {
            b.get();
            Assert.assertTrue(false);
        } catch (CancellationException e) {
            Assert.assertTrue(b.isCancelled());
            Assert.assertTrue(b.isDone());
        }
    }

    @Test
    public void TestCancellableWithTimeout() throws ExecutionException {
        final long WAIT_TIME = 100;
        final long TOLERANCE = 10;
        CancellableThread b = new CancellableThread(status -> {
            while (!status.isCancelled())
                BetterThread.Current.yield();
        });
        b.start();
        long begin = System.currentTimeMillis();
        // Will not join if cancel does not work
        try {
            b.get(WAIT_TIME, TimeUnit.MILLISECONDS);
            Assert.assertTrue(false);
        } catch (TimeoutException e) {
            long finish = System.currentTimeMillis();
            long delta = WAIT_TIME - finish + begin;
            // We wanted roughly 100 milliseconds waited
            Assert.assertTrue(delta > -TOLERANCE && delta < TOLERANCE);
            Assert.assertFalse(b.isCancelled());
            Assert.assertFalse(b.isDone());
        }
        b.cancel();
    }
}
