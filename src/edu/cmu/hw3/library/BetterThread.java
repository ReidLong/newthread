package edu.cmu.hw3.library;

import java.util.Arrays;
import java.util.EnumMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * This is the prototype for the proposed {@code java.lang.Thread} class
 * modifications. For now it is built on top of {@code java.lang.Thread},
 * but a real implementation would be built on top of the same primitives
 * inside {@code java.lang.Thread}
 *
 * <p>Subclasses of {@code BetterThread} are permitted. The expected subclasses
 * will extend on the better thread features as in {@code CancellableThread}.
 * Subclasses must provide a {@code Runnable} which will act as the main loop
 * of the subclasses thread. If a subclass overrides {@linkplain #start start
 * } then the subclass must invoke {@code super.start()} to ensure the {@code
 * BetterThread} state is consistent. The subclass should assume that
 * {@linkplain #toString toString} might call any of the {@code BetterThread}
 * accessors. As such, accessors in the subclass should not depend on parsing
 * the results of {@linkplain BetterThread#toString toString}.</p>
 */
public class BetterThread {

    /* package private */ final InternalHack thread;
    private UncaughtExceptionHandler eh;

    /**
     * Allocates a new {@code BetterThread} object so that it has {@code
     * target} as its run object. This constructor has the
     * same effect as {@linkplain #BetterThread(Runnable,String) BetterThread}
     * {@code (target, null)}.
     *
     * @param  target
     *         the object whose {@code run} method is invoked when this thread
     *         is started. If {@code null}, the thread does nothing.
     */
    public BetterThread(Runnable target) {
        this(target, null);
    }

    /**
     * Allocates a new {@code BetterThread} object so that it has {@code target}
     * as its run object and has the specified {@code name} as its name
     *
     * <p>The newly created thread is initially marked as being a daemon
     * thread if and only if the thread creating it is currently marked
     * as a daemon thread. The method {@linkplain #setDaemon setDaemon}
     * may be used to change whether or not a thread is a daemon.
     *
     * @param  target
     *         the object whose {@code run} method is invoked when this thread
     *         is started. If {@code null}, the thread does nothing
     *
     * @param  name
     *         the name of the new thread, or null for a default name
     */
    public BetterThread(Runnable target, String name) {
        this(target, name, 0);
    }

    /**
     * Allocates a new {@code BetterThread} object so that it has {@code target}
     * as its run object and has the specified {@code name} as its name and has
     * the specified <i>stack size</i>.
     *
     * <p>This constructor is identical to {@link
     * #BetterThread(Runnable,String)} with the exception of the fact
     * that it allows the thread stack size to be specified.  The stack size
     * is the approximate number of bytes of address space that the virtual
     * machine is to allocate for this thread's stack.  <b>The effect of the
     * {@code stackSize} parameter, if any, is highly platform dependent.</b>
     *
     * <p>On some platforms, specifying a higher value for the
     * {@code stackSize} parameter may allow a thread to achieve greater
     * recursion depth before throwing a {@link StackOverflowError}.
     * Similarly, specifying a lower value may allow a greater number of
     * threads to exist concurrently without throwing an {@link
     * OutOfMemoryError} (or other internal error).  The details of
     * the relationship between the value of the {@code stackSize} parameter
     * and the maximum recursion depth and concurrency level are
     * platform-dependent.  <b>On some platforms, the value of the
     * {@code stackSize} parameter may have no effect whatsoever.</b>
     *
     * <p>The virtual machine is free to treat the {@code stackSize}
     * parameter as a suggestion.  If the specified value is unreasonably low
     * for the platform, the virtual machine may instead use some
     * platform-specific minimum value; if the specified value is unreasonably
     * high, the virtual machine may instead use some platform-specific
     * maximum.  Likewise, the virtual machine is free to round the specified
     * value up or down as it sees fit (or to ignore it completely).
     *
     * <p>Specifying a value of zero for the {@code stackSize} parameter will
     * cause this constructor to behave exactly like the
     * {@code BetterThread(Runnable, String)} constructor.
     *
     * <p><i>Due to the platform-dependent nature of the behavior of this
     * constructor, extreme care should be exercised in its use.
     * The thread stack size necessary to perform a given computation will
     * likely vary from one JRE implementation to another.  In light of this
     * variation, careful tuning of the stack size parameter may be required,
     * and the tuning may need to be repeated for each JRE implementation on
     * which an application is to run.</i>

     * @param  target
     *         the object whose {@code run} method is invoked when this thread
     *         is started. If {@code null}, the thread does nothing
     *
     * @param  name
     *         the name of the new thread, or null for a default name
     *
     * @param  stackSize
     *         the desired stack size for the new thread, or zero to indicate
     *         that this parameter is to be ignored.
     */
    public BetterThread(Runnable target, String name, long stackSize) {
        thread = new InternalHack(this, target, name, stackSize);
    }

    /**
     * Allocates a new {@code BetterThread} object so that is has {@code
     * target} as its run object and the specified stack size. This
     * constructor has the same effect as
     * {@linkplain #BetterThread(Runnable,String,long)
     * BetterThread}
     * {@code (target, null, stackSize)}.
     *
     * @param  target
     *         the object whose {@code run} method is invoked when this thread
     *         is started. If {@code null}, the thread does nothing.
     *
     * @param  stackSize
     *         the desired stack size for the new thread, or zero to indicate
     *         that this parameter is to be ignored.
     */
    public BetterThread(Runnable target, long stackSize) {
        this(target, null, stackSize);
    }

    private static volatile AtomicInteger nextId = new AtomicInteger(1);
    private static String selectName(String name) {
        if(name == null) {
            name = String.format("BetterThread-%d", nextId.incrementAndGet());
        }
        return name;
    }

    /**
     * This is an internal wrapper class to help implement BetterThreads on
     * top of java.lang.Thread. In a real system, the
     * BetterThread implementation would
     * likely be replaced with native methods.
     */
    /* package private */ class InternalHack extends Thread {
        private final BetterThread thread;
        /* package private */ final Runnable target;
        InternalHack(BetterThread thread, Runnable target, String name,
                            long stackSize) {
            super(null, target, selectName(name), stackSize);
            this.target = target;
            this.thread = thread;
        }
    }

    /**
     * Causes this thread to begin execution.
     *
     * The result is that two threads are running concurrently: the current thread (which returns from the call
     * to the {@code start} method) and the other thread (which executes its runnable). It is never legal to start a
     * thread more than once. In particular, a thread may not be restarted once it has completed execution.
     *
     * @throws IllegalThreadStateException if the thread was already started
     */
    public void start() {
        thread.start();
    }

    /**
     * Waits for this thread to die (indefinitely).
     */
    public final void join() {
        try {
            thread.join();
        } catch (InterruptedException e) {
            throw new AssertionError("Unexpected InterruptedException", e);
        }
    }

    /**
     * Waits at most {@code time} many units of the given {@code TimeUnit}
     * @param time the time duration in the given {@code unit} that the method will wait for at most
     * @param unit the unit of the {@code time} argument
     */
    public final void join(long time, TimeUnit unit) {
        try {
            // Kinda inversion
            unit.timedJoin(thread, time);
        } catch (InterruptedException e) {
            throw new AssertionError("Unexpected InterruptedException", e);
        }
    }

    /**
     * Tests if this thread is alive. A thread is alive if it has been started and has not yet died.
     * @return true if this thread is alive, false otherwise.
     */
    public final boolean isAlive() {
        return thread.isAlive();
    }

    /**
     * Tests if this thread is a daemon thread
     * @return true if this thread is a daemon thread, false otherwise
     * @see #setDaemon(boolean)
     */
    public final boolean isDaemon() {
        return thread.isDaemon();
    }

    /**
     * Marks this thread as either a daemon thread or a user thread. The JVM exits when the only threads running are
     * all daemon threads. The method must be invoked before the thread is started.
     * @param on if true, marks this thread as a daemon thread
     * @return self-reference for method chaining
     *
     * @throws IllegalThreadStateException if this thread is alive
     */
    public final BetterThread setDaemon(boolean on) {
        thread.setDaemon(on);
        return this;
    }

    /**
     * Returns the identifer of this BetterThread. THe thread ID is a positive long number generated when this thread
     * was created. The thread ID is unique and remains unchanged during its lifetime. When a thread is terminated, this
     * thread ID maybe reused.
     * @return this thread's ID
     */
    public long getId() {
        return thread.getId();
    }

    /**
     * Returns this thread's name
     * @return this thread's name
     * @see #getName()
     */
    public final String getName() {
        return thread.getName();
    }


    /**
     * Changes the name of this thread to be equal to the argument {@code name}
     * @param name the new name for this thread
     * @return self-reference for method chaining
     */
    public final BetterThread setName(String name) {
        thread.setName(name);
        return this;
    }

    /**
     * Returns an array of stack trace elements representing the stack dump of this thread. This method will return a
     * zero-length array if this thread has not started, has started but has not yet been scheduled to run by the
     * system, or has terminated. If the returned array is of non-zero length then the first element of the array
     * represents the top of the stack, which is the most recent method invocation in the sequence. The last element of
     * the array represents the bottom of the stack, which is the least recent method invocation in the sequence.
     *
     * Some virtual machines may, under some circumstances, omit one or more stack frames from the stack trace. In the
     * extreme case, a virtual machine that has no stack trace information concerning this thread is permitted to return
     * a zero-length array from this method.
     *
     * @return an array of {@code StackTraceElement}, each represents one stack frame.
     */
    public StackTraceElement[] getStackTrace() {
        return thread.getStackTrace();
    }

    /**
     * Returns a string representation of this thread, including the thread's
     * name and id
     * @return a string representation of this thread.
     */
    @Override
    public String toString() {
        return String.format("BetterThread[%s,%d]", getName(), getId());
    }

    /**
     * Returns the state of this thread. This method is designed for use in monitoring of the system state. not for
     * synchronization control.
     * @return the thread's state
     */
    public State getState() {
        return State.of(thread.getState());
    }

    /**
     * Returns the handler invoked when this thread abruptly terminates due to an uncaught exception. If no handler is
     * configured, or if the thread has terminated, null is returned.
     * @return the handler invoked when this thread abruptly terminates due to an uncaught exception
     */
    public UncaughtExceptionHandler getUncaughtExceptionHandler() {
        return eh;
    }

    /**
     * Set the handler invoked when this thread abruptly terminates due to an uncaught exception. A thread can take
     * full control of how it responds to uncaught exceptions by having its uncaught exception handler explicit set.
     *
     * @param eh the object to use as the thread's uncaught exception handler. If null then this thread has no explicit
     *           handler.
     * @return self-reference for method chaining
     */
    public BetterThread setUncaughtExceptionHandler(UncaughtExceptionHandler eh) {
        this.eh = eh;
        thread.setUncaughtExceptionHandler((t, e) -> eh.uncaughtException(this, e));
        return this;
    }

    /**
     * A thread execution state. A thread can be in one of the following states:
     * <ul>
     *     <li>{@link #NEW} A thread that has not yet started is in this
     *     state</li>
     *     <li>{@link #RUNNABLE} A thread executing in the JVM is in this
     *     state</li>
     *     <li>{@link #BLOCKED} A thread that is blocked waiting for a
     *     resource is in this state</li>
     *     <li>{@link #TERMINATED} A thread that has exited is in this state
     *     </li>
     * </ul>
     * @see #getState()
     */
    public enum State {
        /**
         * A thread which has not yet started
         */
        NEW,
        /**
         * A thread executing in the JVM
         */
        RUNNABLE,
        /**
         * A thread that is blocked waiting for a resource
         */
        BLOCKED,
        /**
         * A thread that has exited
         */
        TERMINATED;

        private static State of(Thread.State threadState) {
            switch(threadState) {
                case NEW: return NEW;
                case RUNNABLE: return RUNNABLE;
                case WAITING: /* fall through */
                case TIMED_WAITING: /* fall through */
                case BLOCKED: return BLOCKED;
                case TERMINATED: return TERMINATED;
                default:
                    throw new AssertionError(String.format("Unknown thread " +
                            "state: %d", threadState));
            }
        }
    }

    /**
     * Interface for handlers invoked when a {@code Thread} abruptly
     * terminates due to an uncaught exception. When a
     * thread is about to terminate due to an uncaught exception the JVM will invoke the handler's
     * {@code uncaughtException} method, passing the thread and the exception as arguments.
     *
     * @see #setUncaughtExceptionHandler(UncaughtExceptionHandler)
     * @see #getUncaughtExceptionHandler()
     *
     */
    public interface UncaughtExceptionHandler {
        /**
         * Method invoked when the given thread terminates due to the given uncaught exception. Any exception thrown
         * by this method will be ignored by the JVM.
         * @param t the thread
         * @param e the exception
         */
        void uncaughtException(BetterThread t, Throwable e);
    }

    /**
     * Utility class that encapsulates methods for the current running caller thread.
     */
    public static final class Current {
        private Current() { /* Uninstantiable */}

        /**
         * Causes the currently executing thread to sleep (temporarily
         * cease execution) for the specified amount of
         * time in the given unit, subject to the precision and accuracy of system timers and schedulers. The thread
         * does not lose ownership of any monitors.
         * @param time the duration of time, in the given unit, to sleep for
         * @param unit the unit for the given time.
         */
        public static void sleep(long time, TimeUnit unit) {
            try {
                Thread.sleep(unit.toMillis(time));
            } catch (InterruptedException e) {
                throw new AssertionError("Unexpected InterruptedException", e);
            }
        }

        /**
         * Returns the {@code BetterThread} object of the caller thread
         * @return the current instance or null if the current Thread is not
         * a {@code BetterThread}
         */
        public static BetterThread instance() {
            Thread current = Thread.currentThread();
            if(current instanceof  InternalHack) {
                InternalHack hack = (InternalHack)current;
                return hack.thread;
            } else {
                return null;
            }
        }

        /**
         * Prints a stack trace of the current thread to the standard error stream. This method is used only for
         * debugging.
         *
         * @see Throwable#printStackTrace()
         */
        public static void dumpStack() {
            Thread.dumpStack();
        }

        /**
         * Returns true if and only if the current thread holds the monitor lock on the specified object. This method
         * is designed to allow a program to assert that the current thread already holds a specified lock:
         * <code>
         *     assert BetterThread.Current.holdsLock(obj);
         * </code>
         * @param obj the object on which to test lock ownership
         * @return true if the current thread holds the monitor lock on the specified object
         * @throws NullPointerException if obj is null
         */
        public static boolean holdsLock​(Object obj) {
            return Thread.holdsLock(obj);
        }

        /**
         * A hint to the scheduler that the current thread is willing to
         * yield its current use of a processor. The
         * scheduler is free to ignore this hint.
         *
         * Yield is a heuristic attempt to improve relative progression between threads that would otherwise
         * over-utilize a CPU. Its use should be combined with detailed profiling and benchmarking to ensure that it
         * actually has the desired effect.
         *
         * It is rarely appropriate to use this method. It may be useful for debugging or testing purposes, where it
         * may help to reproduce bugs due to race conditions. It may also be useful when the designing concurrency
         * control constructs such as the ones in the {@link java.util.concurrent.locks} package.
         */
        public static void yield() {
            Thread.yield();
        }


    }

}
