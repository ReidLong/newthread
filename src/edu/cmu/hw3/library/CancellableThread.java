package edu.cmu.hw3.library;

import java.util.concurrent.*;
import java.util.function.Consumer;

/**
 * This is the prototype for the proposed {@code java.lang.Thread} class
 * modifications. A {@code CancellableThread} demonstrates how to create an
 * "interruptable" or cancelable thread. We propose splitting simple threads
 * and cancelable threads into different classes since they provide distinct
 * behaviors and have different contacts for the programmer.
 *
 * <p>Instead of taking an {@code Runnable} target, a CancellableThread takes a
 * {@code Consumer<CancellableThread.Status>} which makes it clear to the
 * programmer that this isn't a simple thread.</p>
 *
 * This is an example of a simple {@code CancellableThread} target
 * <pre>{@code
 *       CancellableThread blinker = new CancellableThread((status) -> {
 *             while(!status.isCancelled()) {
 *                 BetterThread.Current.sleep(1000, TimeUnit.MILLISECONDS);
 *                 System.out.println("repainting screen");
 *             }
 *         });
 * }</pre>
 *
 */
public final class CancellableThread extends BetterThread implements Future<Void> {

    /**
     * Allocates a new {@code CancellableThread} object so that it has {@code
     * target} as its run object. This constructor has the same effect as
     * {@linkplain #CancellableThread(Consumer,String)
     * CancellableThread}{@code (target, null)}.
     *
     * @param  target
     *         the object whose {@code accept} method is invoked when this
     *         thread
     *         is started. If {@code null}, the thread does nothing.
     */
    public CancellableThread(Consumer<Status> target) {
        this(target, null);
    }

    /**
     * Allocates a new {@code CancellableThread} object so that it has {@code
     * target} as its run object and has the specified {@code name} as its name
     *
     * @param  target
     *         the object whose {@code accept} method is invoked when this
     *         thread
     *         is started. If {@code null}, the thread does nothing.
     *
     * @param name the name of the new thread, or null for a default name
     *
     * @see BetterThread#BetterThread(Runnable, String)
     */
    public CancellableThread(Consumer<Status> target, String name) {
        this(target, name, 0);
    }

    /**
     * Allocates a new {@code CancellableThread} object so that it has {@code
     * target} as its run object and has the specified {@code name} as its name and has
     * the specified stack size.
     *
     * @param  target
     *         the object whose {@code accept} method is invoked when this
     *         thread
     *         is started. If {@code null}, the thread does nothing.
     *
     * @param name the name of the new thread, or null for a default name
     *
     * @param  stackSize
     *         the desired stack size for the new thread, or zero to indicate
     *         that this parameter is to be ignored.
     *
     * @see BetterThread#BetterThread(Runnable, String, long)
     */
    public CancellableThread(Consumer<Status> target, String name,
                             long stackSize) {
        super(new ThreadRunner(target), name, stackSize);
    }


    /**
     * Allocates a new {@code CancellableThread} object so that it has
     * {@code target} as its run object, and the specified stack size. This
     * constructor has the same effect as
     * {@linkplain #CancellableThread(Consumer,String,long)
     * CancellableThread}{@code (target, null, stackSize)}.
     *
     * @param  target
     *         the object whose {@code accept} method is invoked when this
     *         thread
     *         is started. If {@code null}, the thread does nothing.
     *
     * @param  stackSize
     *         the desired stack size for the new thread, or zero to indicate
     *         that this parameter is to be ignored.
     */
    public CancellableThread(Consumer<Status> target, long stackSize) {
        this(target, null, stackSize);
    }

    /**
     * This is the internal implementation of the CancellableThread. We
     * expect that other implementers of subclasses of {@code BetterThread}
     * will follow similar patterns
     */
    private static class ThreadRunner implements Runnable {

        private final Consumer<Status> block;
        private final Status status = new Status();

        private ThreadRunner(Consumer<Status> block) {
            this.block = block;
        }

        @Override
        public void run() {
            block.accept(status);
        }

        private static ThreadRunner from(BetterThread betterThread) {
            return (ThreadRunner)betterThread.thread.target;
        }
    }

    /**
     * This {@code Status} object represents the cancellation status of the
     * thread. A running cancellable thread should inspect {@code
     * isCancelled} at safe checkpoints and then shutdown gracefully if the
     * thread has been cancelled.
     */
    public static class Status {
        private Status() { /* internal only */}
        private volatile boolean cancelled = false;

        /**
         * Tests if this thread has been cancelled.
         * @return true if the thread has been cancelled, false otherwise.
         */
        public boolean isCancelled() {
            return cancelled;
        }
    }


    /**
     * Attempts to cancel execution of the thread safely.
     *
     * This is a one-way trip. Once a thread is cancelled it will forever be
     * cancelled.
     */
    public void cancel() {
        cancel(true);
    }

    /**
     * Tests if this thread is cancelled.
     * @return true if the instance has been cancelled
     */
    @Override
    public boolean isCancelled() {
        return ThreadRunner.from(this).status.isCancelled();
    }


    /**
     * Attempts to cancel execution of the thread safely.
     * @see Future#cancel(boolean)
     */
    @Override
    public boolean cancel(boolean mayInterruptIfRunning) {
        boolean result = isCancelled() || isDone();
        if (!result && mayInterruptIfRunning) {
            ThreadRunner.from(this).status.cancelled = true;
        }
        return !result && mayInterruptIfRunning;
    }

    /**
     * Tests if this thread is finished executing
     * @see Future#isDone()
     */
    @Override
    public boolean isDone() {
        return getState() == BetterThread.State.TERMINATED;
    }

    /**
     * Waits if necessary for the thread to terminate.
     * @return null
     * @see Future#get()
     */
    @Override
    public Void get() throws ExecutionException {
        try {
            join();
        } catch (Exception e) {
            throw new ExecutionException(e);
        }
        if (isCancelled()) throw new CancellationException();
        return null;
    }

    /**
     * Waits if necessary for at most the given time for the thread to
     * terminate.
     * @return null
     * @see Future#get(long, TimeUnit)
     */
    @Override
    public Void get(long timeout, TimeUnit unit) throws ExecutionException, TimeoutException {
        try {
            join(timeout, unit);
        } catch (Exception e) {
            throw new ExecutionException(e);
        }
        if (isCancelled()) throw new CancellationException();
        if (getState() != BetterThread.State.TERMINATED) throw new TimeoutException();
        return null;
    }
}
